require 'active_support/core_ext/numeric/time'

module Cleanup
  module ApiHelper
    def stale_merge_requests(project_id, stale_mr_days)
      total_retrieved = 0
      mr_hashes = []
      stale_at_date = stale_mr_days.days.ago.utc
      stale_at_date_api_option = stale_at_date.iso8601
      page_size = 20
      api_options = {
        per_page: page_size,
        # view: 'simple', # This makes it MUCH, MUCH faster
        state: 'opened',
        updated_before: stale_at_date_api_option,
        order_by: 'updated_at',
        sort: 'desc'
      }

      paginated_mrs = Gitlab.merge_requests(project_id, api_options)
      paginated_mrs.paginate_with_limit(pagination_limit) do |item|
        total_retrieved += 1
        item_hash = item.to_hash

        mr_hash = {
          id: item_hash.fetch('id'),
          iid: item_hash.fetch('iid'),
          state: item_hash.fetch('state'),
          updated_at: Time.parse(item.to_hash.fetch('updated_at')),
          labels: item_hash.fetch('labels'),
          source_branch: item_hash.fetch('source_branch')
        }

        mr_hashes << mr_hash

        puts "Found #{mr_hashes.length} Merge Requests older than #{stale_mr_days} days in state 'opened'..." if (mr_hashes.length % page_size).zero?
      end

      mr_hashes
    end

    def add_note_to_merge_request(project_id, mr_iid, note_body, dry_run)
      if dry_run
        # Still do another actual request to test rate limiting with real requests against real data
        Gitlab.merge_request(project_id, mr_iid)
      else
        Gitlab.create_merge_request_note(project_id, mr_iid, note_body)
      end
    end

    def add_label_to_merge_request(project_id, mr_iid, existing_labels, label_title, dry_run)
      if dry_run
        # Still do another actual request to test rate limiting with real requests against real data
        Gitlab.merge_request(project_id, mr_iid)
      else
        Gitlab.update_merge_request(project_id, mr_iid, { labels: existing_labels << label_title })
      end
    end

    private

    def pagination_limit
      max_items = ENV['CLEANUP_MAX_ITEMS_TO_PROCESS']
      max_items ? max_items.to_i : 10_000
    end
  end
end
